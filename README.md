# GitLab CI template for SonarQube

This project implements a GitLab CI/CD template to continuously inspect your codebase with [SonarQube](https://www.sonarqube.org/).

SonarQube is a **Code Quality and Security tool** that helps you analyse your source code and detect quality issues or
security vulnerabilities as early as possible.

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/sonar/gitlab-ci-sonar@4.2.3
    # 2: set/override component inputs
    inputs:
      host-url: https://sonarqube.acme.host # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/sonar'
    ref: '4.2.3'
    file: '/templates/gitlab-ci-sonar.yml'

variables:
  # 2: set/override template variables
  SONAR_HOST_URL: https://sonarqube.acme.host # ⚠ this is only an example
```

## SonarQube analysis job

This job performs a SonarQube analysis of your code.

It is bound to the `test` stage, and uses the following variables:

| Input / Variable | Description                     | Default value |
| ------------------------ | ------------------------------- | ----------------------------- |
| `scanner-image` / `SONAR_SCANNER_IMAGE` | The Docker image used to run [sonar-scanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/) | `registry.hub.docker.com/sonarsource/sonar-scanner-cli:latest` |
| `host-url` / `SONAR_HOST_URL` | SonarQube server url            | _none_ (disabled) |
| `project-key` / `SONAR_PROJECT_KEY` | SonarQube Project Key (might also be set in the `sonar-project.properties` file) | fallbacks to `$CI_PROJECT_PATH_SLUG` (see below) |
| `project-name` / `SONAR_PROJECT_NAME` | SonarQube Project Name (might also be set in the `sonar-project.properties` file) | fallbacks to `$CI_PROJECT_PATH` (see below) |
| :lock: `SONAR_TOKEN`     | SonarQube authentication [token](https://docs.sonarqube.org/latest/user-guide/user-token/) (depends on your authentication method) | _none_ |
| :lock: `SONAR_LOGIN`     | SonarQube login (depends on your authentication method)                | _none_ |
| :lock: `SONAR_PASSWORD`  | SonarQube password (depends on your authentication method)             | _none_ |
| `base-args` / `SONAR_BASE_ARGS` | SonarQube [analysis arguments](https://docs.sonarqube.org/latest/analysis/analysis-parameters/) | `-Dsonar.links.homepage=${CI_PROJECT_URL} -Dsonar.links.ci=${CI_PROJECT_URL}/-/pipelines -Dsonar.links.issue=${CI_PROJECT_URL}/-/issues` |
| `quality-gate-enabled` / `SONAR_QUALITY_GATE_ENABLED` | Set to `true` to enable SonarQube [Quality Gate](https://docs.sonarqube.org/latest/user-guide/quality-gates/) verification.<br/>_Uses `sonar.qualitygate.wait` parameter ([see doc](https://docs.sonarqube.org/latest/analysis/ci-integration-overview/#header-1))._ | _none_ (disabled) |

### Automatic Branch Analysis & Merge Request Analysis

This template relies on SonarScanner's [GitLab integration](https://docs.sonarqube.org/latest/analysis/gitlab-integration), that is able to auto-detect whether to launch Branch Analysis or Merge Request Analysis
from GitLab's environment variables.

:warning: This feature also depends on your SonarQube server version and license.
If using Community Edition, you'll have to install the [sonarqube-community-branch-plugin](https://github.com/mc1arke/sonarqube-community-branch-plugin) to enable automatic Branch & Merge Request analysis (only works from SonarQube version 8).

:warning: Merge Request Analysis only works if you're running [Merge Request pipeline](https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines) strategy (default).

### Configuring SonarQube project key, project name and other parameters

You shall define your SonarQube project key and project name in a `sonar-project.properties` file located at the root of your repository (as respectively `sonar.projectKey` and `sonar.projectName` entries), although they might alternately be set as `$SONAR_PROJECT_KEY` and `$SONAR_PROJECT_NAME` variables.

Note that when not explictly set, the template will use `$CI_PROJECT_PATH_SLUG` and `$CI_PROJECT_PATH` as fallback project key and project name.

The `sonar-project.properties` file is also the recommended way to configure other SonarQube [analysis parameters](https://docs.sonarqube.org/latest/analysis/analysis-parameters/)
as well as [language specific parameters](https://docs.sonarqube.org/latest/analyzing-source-code/test-coverage/overview/).

Each _to-be-continuous_ build template shall briefly document the supported language-specific SonarQube parameters.

## Variants

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Input / Variable | Description                            | Default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_VAULT_IMAGE` | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest` |
| `vault-base-url` / `VAULT_BASE_URL` | The Vault server base API url          | _none_ |
| `vault-oidc-aud` / `VAULT_OIDC_AUD` | The `aud` claim for the JWT | `$CI_SERVER_URL` |
| :lock: `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID | **must be defined** |
| :lock: `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID | **must be defined** |

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Parameter                        | Description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - component: gitlab.com/to-be-continuous/sonar/gitlab-ci-sonar@4.2.3
  # Vault variant
  - component: gitlab.com/to-be-continuous/sonar/gitlab-ci-sonar-vault@4.2.3
    inputs:
       # audience claim for JWT
      vault-oidc-aud: "https://vault.acme.host"
      vault-base-url: "https://vault.acme.host/v1"

variables:
  # Secrets managed by Vault
  SONAR_TOKEN: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-app/sonar?field=token"
  # $VAULT_ROLE_ID and $VAULT_SECRET_ID defined as a secret CI/CD variable
```
