## [4.2.3](https://gitlab.com/to-be-continuous/sonar/compare/4.2.2...4.2.3) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([3973282](https://gitlab.com/to-be-continuous/sonar/commit/397328287bfc50c0ccc93856597e699210dbcbfc))

## [4.2.2](https://gitlab.com/to-be-continuous/sonar/compare/4.2.1...4.2.2) (2024-04-03)


### Bug Fixes

* **vault:** use vault-secrets-provider's "latest" image tag ([44b9861](https://gitlab.com/to-be-continuous/sonar/commit/44b9861eec703a5f3f4c8dbbbbbf15a85cf75144))

## [4.2.1](https://gitlab.com/to-be-continuous/sonar/compare/4.2.0...4.2.1) (2024-1-30)


### Bug Fixes

* sanitize variable substitution pattern ([66c257c](https://gitlab.com/to-be-continuous/sonar/commit/66c257c8c4763c1c6e08d0b56a2c225f26270cc3)), closes [#21](https://gitlab.com/to-be-continuous/sonar/issues/21)

# [4.2.0](https://gitlab.com/to-be-continuous/sonar/compare/4.1.1...4.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([ed6b967](https://gitlab.com/to-be-continuous/sonar/commit/ed6b967c3d5dbba909c0a1c99acfdc9e6af57f53))

## [4.1.1](https://gitlab.com/to-be-continuous/sonar/compare/4.1.0...4.1.1) (2024-1-17)


### Bug Fixes

* support secret evaluation for vault usage ([f4bcd24](https://gitlab.com/to-be-continuous/sonar/commit/f4bcd240ec01f3fc21ea2b8bdee8a81ad22d31e3))

# [4.1.0](https://gitlab.com/to-be-continuous/sonar/compare/4.0.1...4.1.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([674c574](https://gitlab.com/to-be-continuous/sonar/commit/674c5749b84a4d05cffdf76157129872e1d0b09d))

## [4.0.1](https://gitlab.com/to-be-continuous/sonar/compare/4.0.0...4.0.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([5248c27](https://gitlab.com/to-be-continuous/sonar/commit/5248c2783cc1c7850a36d8f3b5c5935e25e8aa97))

# [4.0.0](https://gitlab.com/to-be-continuous/sonar/compare/3.3.0...4.0.0) (2023-08-28)


### Features

* **oidc:** OIDC authentication support now requires explicit configuration (see doc) ([a617e8e](https://gitlab.com/to-be-continuous/sonar/commit/a617e8e48db080d489ac4824149150827d15f03f))


### BREAKING CHANGES

* **oidc:** OIDC authentication support now requires explicit configuration (see doc)

# [3.3.0](https://gitlab.com/to-be-continuous/sonar/compare/3.2.0...3.3.0) (2023-06-15)


### Features

* **vault:** add Vault support (variant) ([1be9259](https://gitlab.com/to-be-continuous/sonar/commit/1be925945bde89b9630897cdc1ace671f7f857de))

# [3.2.0](https://gitlab.com/to-be-continuous/sonar/compare/3.1.1...3.2.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([2f77b8d](https://gitlab.com/to-be-continuous/sonar/commit/2f77b8d724fea656b6abcc92adf865d3ad471065))

## [3.1.1](https://gitlab.com/to-be-continuous/sonar/compare/3.1.0...3.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([5053378](https://gitlab.com/to-be-continuous/sonar/commit/505337882a1e253c828f05c1e769d345c6733e5a))

# [3.1.0](https://gitlab.com/to-be-continuous/sonar/compare/3.0.0...3.1.0) (2022-08-10)


### Features

* add wait for quality gate option ([1c4c9cc](https://gitlab.com/to-be-continuous/sonar/commit/1c4c9ccf1abe0db8033813cc0d2f8d997d03b00e))
* manage separate $SONAR_PROJECT_KEY and $SONAR_PROJECT_NAME ([a0e34f4](https://gitlab.com/to-be-continuous/sonar/commit/a0e34f49762d8ada95c261e5c8df014f394de1f2))
* manage Sonar task cache in GitLab ([c44a43d](https://gitlab.com/to-be-continuous/sonar/commit/c44a43d575c8a81abe972b30aeca5038bb39e0a6))
* migrate $SONAR_AUTH_TOKEN to $SONAR_TOKEN (standard) ([feb5ceb](https://gitlab.com/to-be-continuous/sonar/commit/feb5ceb2c6356586e5d9b32cdc874733b14bc667))
* migrate $SONAR_URL to $SONAR_HOST_URL (standard) ([4abffcf](https://gitlab.com/to-be-continuous/sonar/commit/4abffcf78a9469cd72a8c8536ba5d096cd99986a))
* remove explicit MR analysis ([6a6d28b](https://gitlab.com/to-be-continuous/sonar/commit/6a6d28b840574307ff3d190ed7465f9f5540c3b8))
* remove support of Sonar GitLab plugin (discontinued) ([7e93a03](https://gitlab.com/to-be-continuous/sonar/commit/7e93a031d68ebfc1fc7f0095a0253ca5edc98dc3))

# [3.0.0](https://gitlab.com/to-be-continuous/sonar/compare/2.1.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline rules ([8ac4615](https://gitlab.com/to-be-continuous/sonar/commit/8ac4615400e6e1992a179576d20ee9f220aa117b))
* switch to Merge Request pipelines as default ([48fdc64](https://gitlab.com/to-be-continuous/sonar/commit/48fdc6442ec9a5bf4a09be849f3edbefcd7a6524))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline
* removed SONAR_AUTO_ON_DEV_DISABLED

# [2.1.0](https://gitlab.com/to-be-continuous/sonar/compare/2.0.1...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([8f761df](https://gitlab.com/to-be-continuous/sonar/commit/8f761df6d04fd93b7c679b6e86ea887646de49b5))

## [2.0.1](https://gitlab.com/to-be-continuous/sonar/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([4bfbfe5](https://gitlab.com/to-be-continuous/sonar/commit/4bfbfe596561f67ec522b96079074460d8d7362d))

## [2.0.0](https://gitlab.com/to-be-continuous/sonar/compare/1.3.2...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([b8f2b40](https://gitlab.com/to-be-continuous/sonar/commit/b8f2b40a430565e551a13a70a595df1b9203bede))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.3.2](https://gitlab.com/to-be-continuous/sonar/compare/1.3.1...1.3.2) (2021-06-15)

### Bug Fixes

* prevent shallow git clone (required by Sonar Scanner) ([7a3ba87](https://gitlab.com/to-be-continuous/sonar/commit/7a3ba8764a65ae3ea85928d804a6e93b59c53059))

## [1.3.1](https://gitlab.com/to-be-continuous/sonar/compare/1.3.0...1.3.1) (2021-06-15)

### Bug Fixes

* autodetect MR when a milestone is here ([e85c45e](https://gitlab.com/to-be-continuous/sonar/commit/e85c45e9a43aeb078a7c9dcc9523508e03ba29bd))

## [1.3.0](https://gitlab.com/to-be-continuous/sonar/compare/1.2.0...1.3.0) (2021-06-10)

### Features

* move group ([d8850f2](https://gitlab.com/to-be-continuous/sonar/commit/d8850f2e6cf93c8403632dbb83cad76876c25677))

## [1.2.0](https://gitlab.com/Orange-OpenSource/tbc/sonar/compare/1.1.0...1.2.0) (2021-06-08)

### Features

* **sonar:** autodetect Merge Request from current branch ([139c6d1](https://gitlab.com/Orange-OpenSource/tbc/sonar/commit/139c6d18a2a83976e49bd0cbce10f4b40876d6d8))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/sonar/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([e1071c6](https://gitlab.com/Orange-OpenSource/tbc/sonar/commit/e1071c61264a13e81f11a637b627ec5a3486d06d))

## 1.0.0 (2021-05-06)

### Features

* initial release ([b18ab7c](https://gitlab.com/Orange-OpenSource/tbc/sonar/commit/b18ab7c79f40b7ea91cd26807ab8eebd08d4b332))
